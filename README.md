# hockeystats
hockey statistics module for sopel irc bots, requires no configuraion at all

## Dependencies
* json
* arrow
* requests

## Game tracking
This feature utilizes a recurring query of active game scores and monitors for changes which it then announces within any channel the bot is currently joined to.  Upon disabling the tracking with !nhl_trackoff it clears the list.

## Usage
*All input is in 3 letter team abbreviations except for divisions, which are spelled out in full*

### **!nhl_trackon**

This enables live game tracking

### **!nhl_trackoff**

This disables live game tracking as well as clears any games currently being tracked

### **!nhl_status**

Checks the current state of game tracking

### **!nhl_gametrack MTL**

This adds a game to the tracking list
### **!nhl_gamedetails DAL**

Gets details for a currently active game

`BOS | 6 shots, 4 hits, 2 pim, 0.0/2.0 pp || DAL | 6 shots, 7 hits, 4 pim, 0.0/1.0 pp`

### **!nhl_games**

This shows all games scheduled for the current day

`NYI@MTL 4-3 / DAL@BOS 3-2 Final / ANA@COL 1-3 Final / SJS@LAK 4-1 Final`

### **!nhl_teamstats DAL**

Shows the W/L/OT Loss and points for a specified team

``Dallas Stars GP 45 (25-17-3) 53 PTS``

### **!nhl_lastgame CHI**

Find out when a team last played and the outcome of the game

``2018-01-14 | Detroit Red Wings @ Chicago Blackhawks | 4 - 0 Final``

### **!nhl_nextgame TOR**

Get date and time for a team's next game

``2018-01-16 | St. Louis Blues @ Toronto Maple Leafs at 19:00:00 EST``

### **!nhl_wildcard**

Shows the current wildcard teams for both the Western and Eastern conferences.

``West WC1 - Dallas Stars(77 pts), WC2 - Arizona Coyotes(75 pts) | East WC1 - Carolina Hurricanes(83 pts), WC2 - Columbus Blue Jackets(81 pts)``
### **!nhl_division Metropolitan**

Get division standings

`Washington Capitals (1) 59 pts, Columbus Blue Jackets (2) 53 pts, New Jersey Devils (3) 52 pts`
