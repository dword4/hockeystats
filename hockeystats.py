#!/usr/bin/env python

from sopel import module, tools
import requests
import json
import arrow

global gametrack
global gametrack_list
global gamedata
gametrack_list = []
gametrack = False
gamedata = []

@module.rate(20)
@module.commands('nhl_wildcard')
def wildCards(bot, trigger):
    r = getCurrentWildcards()
    bot.say(r)

@module.rate(20)
@module.commands('nhl_gamedetails')
def gameDetails(bot, trigger):
    """ Gets detailed stats for an in-progress game. usage: !nhl_gamedetails MIN """
    tmpTeam = trigger.group(2)
    teamName = tmpTeam.split()
    try:
        if tmpTeam is not None:
            teamId = getTeamId(teamName[0])
            game_details = getGameDetails(teamId)
            msg = game_details
        else:
            msg = "please specify a correct 3 letter team abbreviation"
    except:
        bot.say("unable to find team id")
    bot.say(msg)

@module.rate(20)
@module.commands('nhl_teamstats')
def teamStats(bot, trigger):
    """ Gets team stats including games played, wins, losses ot. usage: !nhl_teamstats WSH """
    tmpTeam = trigger.group(2)
    teamName = tmpTeam.split()
    try:
        if tmpTeam is not None:
            teamId = getTeamId(teamName[0])
            msg = getTeamStats(teamId)
        else:
            msg = "please search using 3 letter team abbreviation"
    except:
        bot.say("unable to find team id")
    bot.say(msg)
    
@module.rate(20)
@module.commands('nhl_lastgame')
def pGame(bot, trigger):
    """ Gets last game information for a team. usage: !nhl_lastgame TOR """
    tmpTeam = trigger.group(2)
    teamName = tmpTeam.split()
    try:
        if tmpTeam is not None:
            teamId = getTeamId(teamName[0])
            msg = prevGame(teamId)
        else:
            msg = "please search using 3 letter team abbreviation"
    except:
        bot.say("unable to find team id")
    bot.say(msg)

@module.rate(20)
@module.commands('nhl_nextgame')
def nGame(bot, trigger):
    """ Gets next game information for a team. usage: !nhl_nextgame MTL """
    tmpTeam = trigger.group(2)
    teamName = tmpTeam.split()
    try:
        if tmpTeam is not None:
            teamId = getTeamId(teamName[0])
            msg = nextGame(teamId)
        else:
            msg = "please search using 3 letter team abbreviation"
    except:
        bot.say("unable to find team id")
    bot.say(msg)

@module.rate(20)
@module.commands('nhl_division')
def divisionStandings(bot, trigger):
    """ Grab standings for a division: Options: Metro(politan), Atlantic, Central, Pacific. usage: !nhl_division <full division name> """
    tmpDiv = trigger.group(2)
    if tmpDiv == None:
        msg = "Specify a division Metro(politan), Atlantic, Central, Pacific"
    else:
        getStandings()
        try:
            if tmpDiv is not None:
                divName = tmpDiv.split()
                divId = getDivisionId(divName[0])
                getStandings()
                msg = getDivisionStandings(divId)
            else:
                msg = "Options: Metro(politan), Atlantic, Central, Pacific"
        except:
            msg = "Options: Metro(politan), Atlantic, Central, Pacific"
    bot.say(msg)

@module.rate(20)
@module.commands('nhl_games')
def currentGameScores(bot, trigger):
    """ Gets the scores of all games scheduled to start today """
    msg = getCurrentGameScores()
    bot.say(msg)

@module.rate(20)
@module.commands('nhl_abbreviation')
def getAbbrToFullname(bot, trigger):
	""" Get the full name of a 3 letter abbreviation. usage: !nhl_abbreviation WPG """ 
	try:
		tmpAbbr = trigger.group(2)
		divAbbr = tmpAbbr.split()
		fullName = abbrToFullname(divAbbr[0])
		if fullName is False:
			msg = "failure to chooch: specify a correct 3 letter abbreviation"
		else:
			msg = "%s = %s" % (divAbbr[0].upper(), fullName)
		bot.say(msg)
	except:
		bot.say("failure to chooch")
@module.rate(30)
@module.commands('nhl_trackon')
def nhlTrackON(bot, trigger):
    """ enables game tracking. usage: !nhl_trackon """
    global gametrack
    if gametrack == False:
        gametrack = True
        message = "Now tracking game progress"
    else:
        message = "Already tracking game progress"
    bot.say(message)

@module.rate(30)
@module.commands('nhl_trackoff')
def nhlTrackOFF(bot, trigger):
    """ disables and resets game tracking, usage: !nhl_trackoff """
    global gametrack
    global gametrack_list
    global gamedata 
    if gametrack == True:
        gametrack = False
        gametrack_list = []
        gamedata = []
        message = "Game tracking disabled"
    else:
        message = "Game tracking is already disabled"
    bot.say(message)

@module.rate(30)
@module.commands('nhl_status')
def nhlGameStatus(bot, trigger):
    global gametrack
    global gametrack_list
    if gametrack == False:
        message = "Game tracking disabled"
    else:
        message = "Game tracking enabled"
        if len(gametrack_list) > 0:
            print(gametrack_list) 
    bot.say(message)

@module.rate(30)
@module.commands('nhl_gametrack')
def trackGame(bot, trigger):
    # start trackGame()
    global gametrack_list
    msg = ''
    tmpTeam = trigger.group(2)
    teamName = tmpTeam.split()
    try:
        if tmpTeam is not None:
            teamId = getTeamId(teamName[0])
            # this is purely to check if someones bad input
    except:
        msg = "please use 3 letter team abbreviation"
    req = 'https://statsapi.web.nhl.com/api/v1/schedule'

    r = requests.get(req)

    game_list_json = r.text
    game_list = json.loads(game_list_json)

    i = 0
    limit = len(game_list['dates'][0]['games']) - 1
    active_games = []
    
    for game in game_list['dates'][0]['games']:
        # first step - compile list of currently active teams
        print(teamName)
        try:
            gameStatus = game['status']['abstractGameState']
            if gameStatus == 'Live':
                game_id = game['gamePk']
                team_away_id = game['teams']['away']['team']['id']
                team_home_id = game['teams']['home']['team']['id']
                team_away_abbr = getTeamAbbr(team_away_id)
                team_home_abbr = getTeamAbbr(team_home_id)
                print(teamId)
                if teamId == team_home_id or teamId == team_away_id:
                    if game_id not in gametrack_list:
                        active_games.append(game_id)
                        gametrack_list.append(game_id)
                    else:
                        bot.say("sorry, already tracking that game")
                else:
                    pass
        except:
            # nothing to see here folks
            pass

    bot.say(msg)
    # end trackGame()

@module.interval(60)
def checkgames(bot):
    global gametrack
    global gametrack_list
    global gamedata

    channel_list = []
    conn_channels = bot.privileges
    channel_list = [channel for channel in conn_channels]
    if gametrack is True and len(gametrack_list) > 0:
        # first and foremost a little setup work
        if len(gamedata) == 0:
            for game in gametrack_list:
                homescore, awayscore = get_score(game)
                data = [game, homescore, awayscore]
                gamedata.append(data)
        else:
            # now we run actual checks and update
            for game in gametrack_list:
                # lets see if we have any newly added games to track first
                if any(game in data for data in gamedata) == False:
                    # we have a mismatch, lets add it
                    homescore, awayscore = get_score(game)
                    data = [game, homescore, awayscore]
                    gamedata.append(data)
                else:
                    # just check/update
                    for k,m in enumerate(gamedata):
                        if game in m:
                            homescore, awayscore = get_score(game)
                            hometeam, awayteam = get_teams(game)
                            # check for goals now
                            if gamedata[k][1] == homescore and gamedata[k][2] == awayscore:
                                # nothing happened
                                pass
                            elif gamedata[k][1] == homescore and gamedata[k][2] < awayscore:
                                # away team scored
                                msg = "*** GOAL %s ***" % awayteam
                                for channel in channel_list:
                                    bot.msg(channel, msg, 1)
                                # update data
                                gamedata[k] = [game, gamedata[k][1], awayscore]
                            elif gamedata[k][1] < homescore and gamedata[k][2] == awayscore:
                                # hometeam scored
                                msg = "*** GOAL %s ***" % hometeam
                                for channel in channel_list:
                                    bot.msg(channel, msg, 1)
                                # update data
                                gamedata[k] = [game, homescore, gamedata[k][2]]
                                    
    else:
        # nothing to do
        pass

def get_score(gameid):
    req = "https://statsapi.web.nhl.com/api/v1/game/"+str(gameid)+"/linescore"
    
    response = requests.get(req)
    game_json = response.text

    game_data = json.loads(game_json)

    homescore = game_data['teams']['home']['goals']
    awayscore = game_data['teams']['away']['goals']

    return homescore, awayscore


def get_teams(gameid):
    req = "https://statsapi.web.nhl.com/api/v1/game/"+str(gameid)+"/linescore"
    
    response = requests.get(req)
    game_json = response.text

    game_data = json.loads(game_json)

    hometeam = game_data['teams']['home']['team']['name']
    awayteam = game_data['teams']['away']['team']['name']

    return hometeam, awayteam

# team stats (gp, win, loss, ot)
def getTeamStats(teamid):
    req = "https://statsapi.web.nhl.com/api/v1/teams/"+str(teamid)+"/stats"
    r = requests.get(req)
    
    team_stats_json = r.text
    team_stats = json.loads(team_stats_json)
    
    teamName = team_stats['stats'][0]['splits'][0]['team']['name']
    gamesPlayed = team_stats['stats'][0]['splits'][0]['stat']['gamesPlayed']
    wins = team_stats['stats'][0]['splits'][0]['stat']['wins']
    losses = team_stats['stats'][0]['splits'][0]['stat']['losses']
    ot = team_stats['stats'][0]['splits'][0]['stat']['ot']
    pts = team_stats['stats'][0]['splits'][0]['stat']['pts']

    teamStats = "%s GP %s (%s-%s-%s) %s PTS" % (teamName, gamesPlayed, wins, losses, ot, pts)
    return teamStats

# prev game details
def prevGame(teamid):
    req = "https://statsapi.web.nhl.com/api/v1/teams/"+str(teamid)+"?expand=team.schedule.previous"
    r2 = requests.get(req)

    game_last_json = r2.text
    game_last = json.loads(game_last_json)
    date = game_last['teams'][0]['previousGameSchedule']['dates'][0]['date']
    home_team = game_last['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['home']
    away_team = game_last['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['away']

    prevGameDetails = date + " | " +away_team['team']['name'] + " @ " +home_team['team']['name'] + " | " + str(away_team['score']) + " - " + str(home_team['score']) + " Final"
    return prevGameDetails

# next game detail
def nextGame(teamid):
    req = "https://statsapi.web.nhl.com/api/v1/teams/"+str(teamid)+"?expand=team.schedule.next"
    r2 = requests.get(req)

    game_last_json = r2.text
    game_last = json.loads(game_last_json)
    date = game_last['teams'][0]['nextGameSchedule']['dates'][0]['date']
    home_team = game_last['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']
    away_team = game_last['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']
    game_data = game_last['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['gameDate']
    a = arrow.get(game_data)
    nextGameDetails = date + " | " +away_team['team']['name'] + " @ " +home_team['team']['name'] + " at " +str(a.to('US/Eastern').time()) + " EST"
    return nextGameDetails

# get the division id
def getDivision(teamid):
    req = "https://statsapi.web.nhl.com/api/v1/teams/15"
    r = requests.get(req)
    team_info_json = r.text
    team_info = json.loads(team_info_json)
    return(team_info['teams'][0]['division']['id'])

# get team abbreviation from ID
def getTeamAbbr(ID):
    ret = False
    teams = {
            "NJD" : 1,
            "NYI" : 2,
            "NYR" : 3,
            "PHI" : 4,
            "PIT" : 5,
            "BOS" : 6,
            "BUF" : 7,
            "MTL" : 8,
            "OTT" : 9,
            "TOR" : 10,
            "CAR" : 12,
            "FLA" : 13,
            "TBL" : 14,
            "WSH" : 15,
            "CHI" : 16,
            "DET" : 17,
            "NSH" : 18,
            "STL" : 19,
            "CGY" : 20,
            "COL" : 21,
            "EDM" : 22,
            "VAN" : 23,
            "ANA" : 24,
            "DAL" : 25,
            "LAK" : 26,
            "SJS" : 28,
            "CBJ" : 29,
            "MIN" : 30,
            "WPG" : 52,
            "ARI" : 53,
            "VGK" : 54
            }
    for team, teamId in teams.items():
        if teamId == ID:
            ret = team
    if ret == False:
        sub_req = "https://statsapi.web.nhl.com/api/v1/teams/"+str(ID)
        sub_r = requests.get(sub_req)
        team_json = sub_r.text
        team_data = json.loads(team_json)
        ret = team_data['teams'][0]['abbreviation']
    return ret

# get team ID from abbreviation
def getTeamId(abbreviation):
    teams = {
            "NJD" : 1,
            "NYI" : 2,
            "NYR" : 3,
            "PHI" : 4,
            "PIT" : 5,
            "BOS" : 6, 
            "BUF" : 7,
            "MTL" : 8,
            "OTT" : 9,
            "TOR" : 10,
            "CAR" : 12,
            "FLA" : 13,
            "TBL" : 14,
            "WSH" : 15,
            "CHI" : 16,
            "DET" : 17,
            "NSH" : 18,
            "STL" : 19,
            "CGY" : 20,
            "COL" : 21,
            "EDM" : 22,
            "VAN" : 23,
            "ANA" : 24,
            "DAL" : 25,
            "LAK" : 26,
            "SJS" : 28,
            "CBJ" : 29,
            "MIN" : 30,
            "WPG" : 52,
            "ARI" : 53,
            "VGK" : 54
            }
    team = abbreviation.upper()

    if team in teams:
        return teams[team.upper()] 
    else:
        return false

# convert 3 letter abbreviations to full names
def abbrToFullname(abbr):
    teams = {
            "NJD" : "New Jersey Devils",
            "NYI" : "New York Islanders",
            "NYR" : "New York Rangers",
            "PHI" : "Philadelphia Flyers",
            "PIT" : "Pittsburgh Penguins",
            "BOS" : "Boston Bruins",
            "BUF" : "Buffalo Sabres",
            "MTL" : "Montreal Canadiens",
            "OTT" : "Ottowa Senators",
            "TOR" : "Toronto Maple Leafs",
            "CAR" : "Carolina Hurricanes",
            "FLA" : "Florida Panthers",
            "TBL" : "Tampa Bay Lightning",
            "WSH" : "Washington Capitals",
            "CHI" : "Chicago Blackhawks",
            "DET" : "Detroit Red Wings",
            "NSH" : "Nashville Predators",
            "STL" : "St. Louis Blues",
            "CGY" : "Calgary Flames",
            "COL" : "Colorado Avalanche",
            "EDM" : "Edmonton Oilers",
            "VAN" : "Vancouver Canucks",
            "ANA" : "Anaheim Ducks",
            "DAL" : "Dallas Stars",
            "LAK" : "Los Angeles Kings",
            "SJS" : "San Jose Sharkes",
            "CBJ" : "Columbus Blue Jackets",
            "MIN" : "Minnesota Wild",
            "WPG" : "Winnipeg Jets",
            "ARI" : "Arizona Coyotes",
            "VGK" : "Vegas Golden Knights"
            }
    if abbr.upper() in teams.keys():
        return teams[abbr.upper()]
    else:
        return False

# get division id value
def getDivisionId(division):
    divisions = {
            "ATLANTIC" : 17,
            "CENTRAL" : 16,
            "METROPOLITAN" : 18,
            "METRO" : 18,
            "PACIFIC" : 15
            }
    if division.upper() in divisions:
        return divisions[division.upper()]
    else:
        return false

# get league wide standings
def getStandings():
    req = "http://statsapi.web.nhl.com/api/v1/standings"
    r = requests.get(req)
    standings_info_json = r.text
    standings_info = json.loads(standings_info_json)
    #print(standings_info['records'][0]['division']['id'])
    #print(standings_info['records'][0]['division']['name'])
    global divisionData
    divisionData = {}
    for n in standings_info['records']:
        divisionId = n['division']['id']
        i = 1
        division_blob = ''
        for nn in range(0,3):
            pts = n['teamRecords'][nn]['points']
            team_name = n['teamRecords'][nn]['team']['name'] 
            division_blob += "%s (%s) %s pts" % (team_name, i, pts)
            if i == 3:
                pass
            else: 
                division_blob += ", "
            i += 1
        divisionData[divisionId] = division_blob

def getDivisionStandings(did):
    # getStandings() needs to be called to make sure data is fresh
    return divisionData[did] 

# get the current wild-card teams
def getCurrentWildcards():
    req = "http://statsapi.web.nhl.com/api/v1/standings/wildCard"
    r = requests.get(req)

    wildcard_info = json.loads(r.text)

    east_wc1 = wildcard_info['records'][0]['teamRecords'][0]
    east_wc2 = wildcard_info['records'][0]['teamRecords'][1]

    west_wc1 = wildcard_info['records'][1]['teamRecords'][0]
    west_wc2 = wildcard_info['records'][1]['teamRecords'][1]

    msg = "\x02West\x02 WC1 - %s(%s pts), WC2 - %s(%s pts) | \x02East\x02 WC1 - %s(%s pts), WC2 - %s(%s pts)" % (west_wc1['team']['name'], west_wc1['points'], west_wc2['team']['name'], west_wc2['points'], east_wc1['team']['name'], east_wc1['points'],east_wc2['team']['name'], east_wc2['points'])
    return msg

# get the scores of today's games
def getCurrentGameScores():
    req = "https://statsapi.web.nhl.com/api/v1/schedule"
    r = requests.get(req)
    ret = ''

    game_list_json = r.text
    game_list = json.loads(game_list_json)

    i = 0
    limit = len(game_list['dates'][0]['games']) - 1
    for game in game_list['dates'][0]['games']:
        gameStatus = game['status']['abstractGameState']
        if gameStatus == 'Live':
            game_id = game['gamePk'] # this is an int btw
            game_score_request = "https://statsapi.web.nhl.com/api/v1/game/"+str(game_id)+"/linescore"
            game_details = requests.get(game_score_request).json()
            game_period = game_details['currentPeriodOrdinal']

            team_away_id = game['teams']['away']['team']['id']
            team_home_id = game['teams']['home']['team']['id']
            team_away_name = game['teams']['away']['team']['name']
            team_home_name = game['teams']['home']['team']['name']
            team_away_score = game_details['teams']['away']['goals']
            team_home_score = game_details['teams']['home']['goals']
            team_away_abbr = getTeamAbbr(team_away_id)
            team_home_abbr = getTeamAbbr(team_home_id)
            msg = "\x02%s\x02@\x02%s\x02 %s-%s %s" % (team_away_abbr, team_home_abbr, team_away_score, team_home_score, game_period)
            print("live:"+msg)
        elif gameStatus == 'Final':
            game_id = game['gamePk'] # this is an int btw
            game_score_request = "https://statsapi.web.nhl.com/api/v1/game/"+str(game_id)+"/linescore"
            game_details = requests.get(game_score_request).json()

            team_away_name = game['teams']['away']['team']['name']
            team_home_name = game['teams']['home']['team']['name']
            team_away_id = game['teams']['away']['team']['id']
            team_home_id = game['teams']['home']['team']['id']
            team_away_score = game_details['teams']['away']['goals']
            team_home_score = game_details['teams']['home']['goals']
            team_away_abbr = getTeamAbbr(team_away_id)
            team_home_abbr = getTeamAbbr(team_home_id)
            msg = "\x02%s\x02@\x02%s\x02 %s-%s Final" % (team_away_abbr, team_home_abbr, team_away_score, team_home_score)
            print("final:"+msg)
        else:
            game_state = game['status']['detailedState']
            game_time = game['gameDate']
            team_away_name = game['teams']['away']['team']['name']
            team_home_name = game['teams']['home']['team']['name']
            team_away_id = game['teams']['away']['team']['id']
            team_home_id = game['teams']['home']['team']['id']
            team_away_score = game['teams']['away']['score']
            team_home_score = game['teams']['home']['score']
            team_away_abbr = getTeamAbbr(team_away_id)
            team_home_abbr = getTeamAbbr(team_home_id)
            a_game_time = arrow.get(game_time)
            game_time_local = a_game_time.to('US/Eastern').format('HHmm')
            msg = "\x02%s\x02@\x02%s\x02 at %s EST" % (team_away_abbr, team_home_abbr, game_time_local)
            print("other:"+msg)
        if i == 0:
            ret += msg
            ret += " / "
        elif i == limit:
            ret += msg
        else:
            ret += msg
            ret += " / "
        i += 1
    print(ret)
    return ret

# get detailed information for a specific game
def getGameDetails(team):
    # only will get details from an active game, for simplicity sake
    # uses 3 letter abbreviations 

    team_id = team
    req = "https://statsapi.web.nhl.com/api/v1/schedule"
    game_list = requests.get(req).json()
    for game in game_list['dates'][0]['games']:
        gameStatus = game['status']['abstractGameState']
        if gameStatus == 'Live' or gameStatus == 'Final':
            # lets add it to the list
            game_id = game['gamePk'] # this is an int btw
            team_away_id = game['teams']['away']['team']['id']
            team_home_id = game['teams']['home']['team']['id']
            print(team_away_id, team_home_id, team_id)
            if team_id == team_away_id or team_id == team_home_id:
                # found it
                game_details_req = "https://statsapi.web.nhl.com/api/v1/game/"+str(game_id)+"/boxscore"
                game_details = requests.get(game_details_req).json()
                
                team_home_abbr = getTeamAbbr(team_home_id)
                team_away_abbr = getTeamAbbr(team_away_id)
                home_skater_stats = game_details['teams']['home']['teamStats']['teamSkaterStats']
                away_skater_stats = game_details['teams']['away']['teamStats']['teamSkaterStats']

                home_pim = home_skater_stats['pim']
                home_shots = home_skater_stats['shots']
                home_hits = home_skater_stats['hits']
                home_ppg = str(home_skater_stats['powerPlayGoals']) + '/' + str(home_skater_stats['powerPlayOpportunities'])

                #print("%s | %s shots, %s hits, %s pim, %s pp" % (team_home_abbr, home_shots, home_hits, home_pim, home_ppg))
                home_team_info = "\x02%s\x02 | %s shots, %s hits, %s pim, %s pp" % (team_home_abbr, home_shots, home_hits, home_pim, home_ppg)

                away_pim = away_skater_stats['pim']
                away_shots = away_skater_stats['shots']
                away_hits = away_skater_stats['hits']
                away_ppg = str(away_skater_stats['powerPlayGoals']) + '/' + str(away_skater_stats['powerPlayOpportunities'])


                #print("%s | %s shots, %s hits, %s pim, %s pp" % (team_away_abbr, away_shots, away_hits, away_pim, away_ppg))
                away_team_info = "\x02%s\x02 | %s shots, %s hits, %s pim, %s pp" % (team_away_abbr, away_shots, away_hits, away_pim, away_ppg)
                ret = away_team_info + " || " + home_team_info
                return ret
            else:
                pass
